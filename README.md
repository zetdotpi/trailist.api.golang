# README #

This file will be updated as new function will be implemented.

### What is this repository for? ###

* Trailist REST API written in Go using mongodb
* alpha version

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### How to run.

Create a .sh file with this string:
`TR_DB_PASSWORD='trailist' TR_DB_USERNAME='trailist' TR_DB_ADDRESS='localhost' TR_PORT='5001' TR_ADDRESS='localhost' TR_KEYS_PATH='~/trailist/keys' go run main.go`

Where:

* TR_DB_PASSWORD - database password
* TR_DB_USERNAME - database username
* TR_DB_ADDRESS - database address
* TR_PORT - application port
* TR_ADDRESS - ip address, application listens to
* TR_KEYS_PATH - absolute FS path to directory where keys are stored (for jwt)