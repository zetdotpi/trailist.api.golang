package auth

import (
	"crypto/rsa"
	"encoding/json"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"log"
	"net/http"
	"path"
	"time"
	"trailist/models"
	"trailist/utils"
)

const (
	privKeyName = "app.rsa"
	pubKeyName  = "app.rsa.pub"
)

var (
	verifyKey *rsa.PublicKey
	signKey   *rsa.PrivateKey
)

func init() {
	env := utils.NewEnvLoader()
	keysPath, err := env.Get("TR_KEYS_PATH")
	if err != nil {
		panic(err)
	}
	loadKeys(keysPath, privKeyName, pubKeyName)
}

func loadKeys(keysPath, privKeyName, pubKeyName string) {
	log.Print("loading private keyfile")
	privKeyPath := path.Join(keysPath, privKeyName)
	log.Print(privKeyPath)
	signBytes, err := ioutil.ReadFile(privKeyPath)
	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		panic(err)
	}

	log.Print("loading public keyfile")
	pubKeyPath := path.Join(keysPath, pubKeyName)
	log.Print(pubKeyPath)
	verifyBytes, err := ioutil.ReadFile(pubKeyPath)
	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		panic(err)
	}
}

func LoginHandler(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)

	decoder := json.NewDecoder(req.Body)
	var loginData map[string]string
	decoder.Decode(&loginData)
	email := loginData["email"]
	password := loginData["password"]
	var user models.User
	if err := db.C("users").Find(bson.M{"email": email}).One(&user); err != nil {
		http.Error(rw, "User doesn't exist.", http.StatusNotFound)
		return
	}
	if !user.CheckPassword(password) {
		http.Error(rw, "Wrong password.", http.StatusUnauthorized)
		return
	}
	token := jwt.New(jwt.GetSigningMethod("RS256"))
	token.Claims["username"] = user.Nickname
	token.Claims["role"] = user.Role
	token.Claims["exp"] = time.Now().Add(time.Hour * 24 * 14)

	log.Print(token.Claims)
	for n, v := range token.Claims {
		log.Printf("%s\t%s", n, v)
	}

	tokenStr, err := token.SignedString(signKey)

	if err != nil {
		log.Print("Error while generating token")
		log.Print(err)
		http.Error(rw, "Oh god, everything is broken!", http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(rw)
	encoder.Encode(map[string]string{"token": tokenStr})
}

func AuthHandler(next http.Handler) http.Handler {
	fn := func(rw http.ResponseWriter, req *http.Request) {
		token, err := jwt.ParseFromRequest(req, func(token *jwt.Token) (interface{}, error) {
			return verifyKey, nil
		})
		if err != nil {
			log.Print(err)
			http.Error(rw, "Please use token to authenticate.", http.StatusUnauthorized)
			return
		}

		if token.Valid {
			// log.Print(token)
			log.Printf("Authenticated call from user \"%s\"", token.Claims["username"])
			context.Set(req, "claims", token.Claims)
			// context.Set(req, "username", token.Claims["username"])
			next.ServeHTTP(rw, req)
		} else {
			http.Error(rw, "Your token is invalid.", http.StatusUnauthorized)
			return
		}
	}
	return http.HandlerFunc(fn)
}
