package auth

import (
	"bytes"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	. "github.com/smartystreets/goconvey/convey"
	"gopkg.in/mgo.v2"
	"net/http"
	"net/http/httptest"
	"testing"
)

var (
	m       *mux.Router
	req     *http.Request
	err     error
	respRec *httptest.ResponseRecorder
	mongo   *mgo.Session
	db      *mgo.Database
)

func setup() {
	mongo, _ = mgo.Dial("localhost")
	mongo.SetMode(mgo.Monotonic, true)

	db = mongo.DB("trailist")

	m = mux.NewRouter()
	m.HandleFunc("/login/", LoginHandler)
}

func TestInit(t *testing.T) {
	Convey("It just shouldn't fail", t, func() {
		// func loadKeysWrongPath() {
		// 	loadKeys("/whoa/", "app.rsa", "app.rsa.pub")
		// }
		So(func() { loadKeys("/whoa/", "app.rsa", "app.rsa.pub") }, ShouldPanic)
		So(func() { loadKeys("/home/zetdotpi/go/src/trailist/keys/", "wrong.rsa", "pub.app.rsa") }, ShouldPanic)
		So(func() { loadKeys("/home/zetdotpi/go/src/trailist/keys/", "app.rsa", "pub.wrong.rsa") }, ShouldPanic)
		So(func() { loadKeys("/home/zetdotpi/go/src/trailist/keys/", "app.rsa", "app.rsa.pub") }, ShouldNotPanic)
	})
}

func TestLogin(t *testing.T) {
	setup()
	Convey("With right email and password (email=\"test@gmail.com\" and password=\"test\")", t, func() {
		Convey("It should return http.StatusOK and valid token", func() {
			respRec := httptest.NewRecorder()
			data := []byte(`{"email": "test@gmail.com", "password": "test"}`)
			req, _ := http.NewRequest("POST", "/login/", bytes.NewBuffer(data))
			req.Header.Set("Content-Type", "application/json")

			context.Clear(req)
			context.Set(req, "db", db)
			m.ServeHTTP(respRec, req)

			So(respRec.Code, ShouldEqual, http.StatusOK)
			So(respRec.Body, ShouldNotBeEmpty)
		})
	})
	Convey("With right email and wrong password", t, func() {
		Convey("It should return http.StatusUnauthorized", func() {
			respRec := httptest.NewRecorder()
			data := []byte(`{"email": "test@gmail.com", "password": "wrongpass"}`)
			req, _ := http.NewRequest("POST", "/login/", bytes.NewBuffer(data))
			req.Header.Set("Content-Type", "application/json")

			context.Clear(req)
			context.Set(req, "db", db)
			m.ServeHTTP(respRec, req)

			So(respRec.Code, ShouldEqual, http.StatusUnauthorized)
		})

	})
	Convey("With wrong email and pass", t, func() {
		Convey("It should return http.StatusNotFound", func() {
			respRec := httptest.NewRecorder()
			data := []byte(`{"email": "wrong@email.com", "password": "anypass"}`)
			req, _ := http.NewRequest("POST", "/login/", bytes.NewBuffer(data))
			req.Header.Set("Content-Type", "application/json")

			context.Clear(req)
			context.Set(req, "db", db)
			m.ServeHTTP(respRec, req)

			So(respRec.Code, ShouldEqual, http.StatusNotFound)
		})
	})
}
