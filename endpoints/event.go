package endpoints

import (
	"encoding/json"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"time"
	"trailist/models"
)

func EventsGet(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	var events []models.Event
	err := db.C("events").Find(nil).All(&events)
	j, err := json.Marshal(events)
	if err != nil {
		log.Print("Error marshalling events")
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func EventsPost(rw http.ResponseWriter, req *http.Request) {
	currentUser := getCurrentUser(req)
	db := context.Get(req, "db").(*mgo.Database)

	decoder := json.NewDecoder(req.Body)
	var newEvent models.Event
	if err := decoder.Decode(&newEvent); err != nil {
		log.Print(err)
		http.Error(rw, "Cannot decode JSON", http.StatusBadRequest)
		return
	}

	count, _ := db.C("trails").Find(bson.M{"_id": newEvent.TrailID}).Count()

	if count == 0 {
		http.Error(rw, "Trail not found", http.StatusBadRequest)
		return
	}

	newEvent.Id = bson.NewObjectId()
	newEvent.AuthorID = currentUser.Id
	newEvent.CreationDatetime = time.Now()
	newEvent.Participants = nil
	newEvent.Results = nil

	events := db.C("events")
	if err := events.Insert(newEvent); err != nil {
		http.Error(rw, "Cannot save event", http.StatusInternalServerError)
		return
	}

	j, _ := json.Marshal(newEvent)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusCreated)
	rw.Write(j)
}

func EventGet(rw http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	eventId := vars["eventId"]
	db := context.Get(req, "db").(*mgo.Database)

	var event models.Event
	if err := db.C("events").FindId(bson.ObjectIdHex(eventId)).One(&event); err != nil {
		log.Print("Cannot find event")
		http.Error(rw, "Cannot find event", http.StatusNotFound)
		return
	}

	j, _ := json.Marshal(event)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func EventPut(rw http.ResponseWriter, req *http.Request) {
	currentUser := getCurrentUser(req)

	vars := mux.Vars(req)
	eventId := vars["eventId"]

	db := context.Get(req, "db").(*mgo.Database)

	var event models.Event
	if err := db.C("events").FindId(bson.ObjectIdHex(eventId)).One(&event); err != nil {
		http.Error(rw, "Cannot find event", http.StatusNotFound)
		return
	}

	if currentUser.Id != event.AuthorID && currentUser.Role != "admin" {
		http.Error(rw, "Only owner can change event parameters", http.StatusForbidden)
		return
	}

	decoder := json.NewDecoder(req.Body)
	var updatedEvent models.Event
	if err := decoder.Decode(&updatedEvent); err != nil {
		log.Print("Error decoding updatedEvent json")
		panic(err)
	}

	event.Name = updatedEvent.Name
	event.Description = updatedEvent.Description
	event.Types = updatedEvent.Types

	count, _ := db.C("trails").FindId(bson.ObjectId(updatedEvent.TrailID)).Count()
	if count == 0 {
		http.Error(rw, "trail not found", http.StatusInternalServerError)
		return
	}
	event.TrailID = updatedEvent.TrailID

	event.StartDatetime = updatedEvent.StartDatetime

	if err := db.C("events").UpdateId(event.Id, event); err != nil {
		log.Print("Error updating event")
		panic(err)
	}
	j, _ := json.Marshal(event)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func EventDelete(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	vars := mux.Vars(req)
	eventId := vars["eventId"]

	if err := db.C("events").RemoveId(bson.ObjectIdHex(eventId)); err != nil {
		log.Print("Some error while deleting trail")
		switch err.Error() {
		case "not found":
			http.Error(rw, "event not found", http.StatusNotFound)
			return
		}
		http.Error(rw, "something gone terribly wrong", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusGone)
}

func EventResultsGet(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	vars := mux.Vars(req)
	eventId := vars["eventId"]

	var event models.Event
	if err := db.C("events").FindId(bson.ObjectIdHex(eventId)).One(&event); err != nil {
		log.Print("Cannot find event")
		http.Error(rw, "Cannot find event", http.StatusNotFound)
		return
	}

	j, _ := json.Marshal(event.Results)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

// EventResultsPost request must be done after TracksPost request with TrackID
func EventResultsPost(rw http.ResponseWriter, req *http.Request) {
	currentUser := getCurrentUser(req)
	db := context.Get(req, "db").(*mgo.Database)
	vars := mux.Vars(req)
	eventId := vars["eventId"]

	var event models.Event
	if err := db.C("events").FindId(bson.ObjectIdHex(eventId)).One(&event); err != nil {
		log.Print("Cannot find event")
		http.Error(rw, "Cannot find event", http.StatusNotFound)
		return
	}

	decoder := json.NewDecoder(req.Body)
	var result models.EventResult
	if err := decoder.Decode(&result); err != nil {
		log.Print("Error decoding result json")
		panic(err)
	}
	result.UserID = currentUser.Id

	// TODO: check if user result is already in list
	// TODO: check if track belongs to trail of event

	count, _ := db.C("tracks").FindId(bson.ObjectId(result.TrackID)).Count()
	if count == 0 {
		http.Error(rw, "track not found", http.StatusInternalServerError)
		return
	}

	pushResult := bson.M{"$push": bson.M{"results": &result}}
	if err := db.C("events").UpdateId(event.Id, pushResult); err != nil {
		http.Error(rw, "Cannot insert result", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusCreated)
}
