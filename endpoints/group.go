package endpoints

import (
	"encoding/json"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"time"
	"trailist/models"
)

func GroupsGet(rw http.ResponseWriter, req *http.Request) {
	// TODO: add pagination
	db := context.Get(req, "db").(*mgo.Database)

	var groups []models.Group
	err := db.C("groups").Find(nil).All(&groups)

	j, err := json.Marshal(groups)
	if err != nil {
		log.Print("error marshalling groups")
		panic(err)
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func GroupsPost(rw http.ResponseWriter, req *http.Request) {
	currentUser := getCurrentUser(req)
	db := context.Get(req, "db").(*mgo.Database)
	decoder := json.NewDecoder(req.Body)
	var newGroup models.Group
	if err := decoder.Decode(&newGroup); err != nil {
		log.Print("Cannot decode Group json")
		panic(err)
	}
	newGroup.Id = bson.NewObjectId()
	newGroup.CreationDatetime = time.Now()
	// FIXME: for testing purposes only
	newGroup.OwnerID = currentUser.Id
	newGroup.ModeratorsIDs = nil
	newGroup.MembersIDs = nil
	groups := db.C("groups")
	if err := groups.Insert(newGroup); err != nil {
		rw.WriteHeader(http.StatusConflict)
		rw.Write([]byte("Duplicate!"))
		return
	}

	j, _ := json.Marshal(newGroup)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusCreated)
	rw.Write(j)
}

func GroupGet(rw http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	groupName := vars["groupName"]

	db := context.Get(req, "db").(*mgo.Database)

	var group models.Group
	if err := db.C("groups").Find(bson.M{"name": groupName}).One(&group); err != nil {
		log.Print("Some error while finding group")
		panic(err)
	}

	j, _ := json.Marshal(group)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func GroupPut(rw http.ResponseWriter, req *http.Request) {
	currentUser := getCurrentUser(req)

	vars := mux.Vars(req)
	groupName := vars["groupName"]

	db := context.Get(req, "db").(*mgo.Database)

	var group models.Group
	if err := db.C("groups").Find(bson.M{"name": groupName}).One(&group); err != nil {
		log.Print("Some error while finding group")
		panic(err)
	}

	if currentUser.Id != group.OwnerID && currentUser.Role != "admin" {
		rw.WriteHeader(http.StatusForbidden)
		return
	}

	decoder := json.NewDecoder(req.Body)
	var updatedGroup models.Group
	if err := decoder.Decode(&updatedGroup); err != nil {
		log.Print("Error decoding updatedGroup json")
		panic(err)
	}

	group.Name = updatedGroup.Name
	group.IsPrivate = updatedGroup.IsPrivate

	if err := db.C("groups").UpdateId(group.Id, group); err != nil {
		log.Print("Error updating group")
		panic(err)
	}
	j, _ := json.Marshal(group)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func GroupDelete(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	vars := mux.Vars(req)
	groupName := vars["groupName"]

	if err := db.C("groups").Remove(bson.M{"name": groupName}); err != nil {
		log.Print("Cannot delete group")
		panic(err)
	}
	rw.WriteHeader(http.StatusGone)
}
