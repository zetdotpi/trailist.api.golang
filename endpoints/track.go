package endpoints

import (
	"encoding/json"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/ptrv/go-gpx"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"
	"trailist/models"
)

func TracksGet(rw http.ResponseWriter, req *http.Request) {
	// TODO: add pagination
	// currentUser := getCurrentUser(req)
	db := context.Get(req, "db").(*mgo.Database)
	var tracks []models.Track
	err := db.C("tracks").Find(nil).All(&tracks)
	j, err := json.Marshal(tracks)
	if err != nil {
		log.Print("Error marshalling tracks")
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func TracksPost(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	currentUser := getCurrentUser(req)

	req.ParseMultipartForm(32 << 16)
	tf, tfHeader, err := req.FormFile("track")
	defer tf.Close()
	if err != nil {
		log.Print("something wrong with file")
		panic(err)
	}

	if filepath.Ext(tfHeader.Filename) != ".gpx" {
		rw.WriteHeader(http.StatusBadRequest)
		rw.Write([]byte("Only .gpx files are supported now. Your file is not one of those."))
		return
	}

	// TODO: save files in appropriate place, not in tmp O_o
	target, err := os.OpenFile("/tmp/"+tfHeader.Filename, os.O_CREATE|os.O_WRONLY, 0664)

	_, err = io.Copy(target, tf)

	if err != nil {
		log.Print("Error while opening target file in TracksPost handler")
		log.Print(err)
		http.Error(rw, "Error saving file", http.StatusInternalServerError)
		return
	}

	gpxObj, err := gpx.ParseFile("/tmp/" + tfHeader.Filename)
	if err != nil {
		log.Print(err)
		return
	}
	length3d := gpxObj.Length3D()
	up, down := gpxObj.UphillDownhill()
	bounds := gpxObj.Bounds()
	st, ft := gpxObj.TimeBounds()

	newTrack := models.Track{
		bson.NewObjectId(),
		currentUser.Id,
		time.Now(),
		models.TrackFile{
			tfHeader.Filename,
			"/tmp/" + tfHeader.Filename,
		},
		models.TrackSummary{
			ft.Sub(st),
			length3d,
			int32(up),
			int32(down),
			models.Bounds{
				models.NewGeoPoint(bounds.MaxLon, bounds.MaxLat),
				models.NewGeoPoint(bounds.MinLon, bounds.MinLat),
			},
		},
		models.NewGeoPoint((bounds.MaxLon+bounds.MinLon)/2., (bounds.MaxLat+bounds.MinLat)/2),
	}

	tracks := db.C("tracks")
	if err := tracks.Insert(newTrack); err != nil {
		rw.WriteHeader(http.StatusConflict)
		rw.Write([]byte("Duplicate!"))
		return
	}

	j, _ := json.Marshal(newTrack)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusCreated)
	rw.Write(j)
}

func TrackGet(rw http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	trackId := vars["trackId"]
	db := context.Get(req, "db").(*mgo.Database)

	var track models.Track
	if err := db.C("tracks").FindId(bson.ObjectIdHex(trackId)).One(&track); err != nil {
		log.Print("Cannot find track")
		http.Error(rw, "Cannot find track", http.StatusNotFound)
	}

	j, _ := json.Marshal(track)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func TrackDelete(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	vars := mux.Vars(req)
	trackId := vars["trackId"]

	if err := db.C("tracks").Remove(bson.M{"_id": bson.ObjectIdHex(trackId)}); err != nil {
		log.Print("Some error while deleting track")
		switch err.Error() {
		case "not found":
			http.Error(rw, "track not found", http.StatusNotFound)
			return
		}
		http.Error(rw, "something gone terribly wrong", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusGone)
}
