package endpoints

import (
	"encoding/json"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"time"
	"trailist/models"
)

func TrailsGet(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	var trails []models.Trail
	err := db.C("trails").Find(nil).All(&trails)
	j, err := json.Marshal(trails)
	if err != nil {
		log.Print("Error marshalling trails")
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func TrailsPost(rw http.ResponseWriter, req *http.Request) {
	currentUser := getCurrentUser(req)
	db := context.Get(req, "db").(*mgo.Database)

	decoder := json.NewDecoder(req.Body)
	var newTrail models.Trail
	if err := decoder.Decode(&newTrail); err != nil {
		http.Error(rw, "Cannot decode JSON", http.StatusBadRequest)
		return
	}

	count, _ := db.C("tracks").Find(bson.M{"_id": newTrail.Track}).Count()

	if count == 0 {
		http.Error(rw, "Track not found", http.StatusBadRequest)
		return
	}

	newTrail.Id = bson.NewObjectId()
	newTrail.AuthorID = currentUser.Id
	newTrail.CreationDatetime = time.Now()

	trails := db.C("trails")
	if err := trails.Insert(newTrail); err != nil {
		http.Error(rw, "Cannot save trail", http.StatusInternalServerError)
		return
	}

	j, _ := json.Marshal(newTrail)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusCreated)
	rw.Write(j)
}

func TrailGet(rw http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	trailId := vars["trailId"]
	log.Print(trailId)
	db := context.Get(req, "db").(*mgo.Database)

	var trail models.Trail
	if err := db.C("trails").FindId(bson.ObjectIdHex(trailId)).One(&trail); err != nil {
		log.Print("Cannot find trail")
		http.Error(rw, "Cannot find trail", http.StatusNotFound)
		return
	}

	j, _ := json.Marshal(trail)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func TrailPut(rw http.ResponseWriter, req *http.Request) {
	currentUser := getCurrentUser(req)

	vars := mux.Vars(req)
	trailId := vars["trailId"]

	db := context.Get(req, "db").(*mgo.Database)

	var trail models.Trail
	if err := db.C("trails").FindId(bson.ObjectIdHex(trailId)).One(&trail); err != nil {
		http.Error(rw, "Cannot find trail", http.StatusNotFound)
		return
	}

	if currentUser.Id != trail.AuthorID && currentUser.Role != "admin" {
		http.Error(rw, "Only owner can change trail parameters", http.StatusForbidden)
		return
	}

	decoder := json.NewDecoder(req.Body)
	var updatedTrail models.Trail
	if err := decoder.Decode(&updatedTrail); err != nil {
		log.Print("Error decoding updatedTrail json")
		panic(err)
	}

	trail.Description = updatedTrail.Description
	trail.Types = updatedTrail.Types

	if err := db.C("trails").UpdateId(trail.Id, trail); err != nil {
		log.Print("Error updating trail")
		panic(err)
	}
	j, _ := json.Marshal(trail)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)

}

func TrailDelete(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	vars := mux.Vars(req)
	trailId := vars["trailId"]

	if err := db.C("trails").RemoveId(bson.ObjectIdHex(trailId)); err != nil {
		log.Print("Some error while deleting trail")
		switch err.Error() {
		case "not found":
			http.Error(rw, "trail not found", http.StatusNotFound)
			return
		}
		http.Error(rw, "something gone terribly wrong", http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusGone)
}
