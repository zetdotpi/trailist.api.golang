package endpoints

import (
	"encoding/json"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"time"
	"trailist/models"
)

func UsersGet(rw http.ResponseWriter, req *http.Request) {
	// TODO: add pagination
	db := context.Get(req, "db").(*mgo.Database)
	var users []models.User
	err := db.C("users").Find(nil).All(&users)

	j, err := json.Marshal(users)
	if err != nil {
		log.Print("Error on marshalling []User")
		panic(err)
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func UsersPost(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	decoder := json.NewDecoder(req.Body)
	var newUser models.User
	if err := decoder.Decode(&newUser); err != nil {
		log.Print("Cannot decode User json")
		panic(err)
	}
	newUser.Id = bson.NewObjectId()
	newUser.RegistrationDatetime = time.Now()
	// FIXME: for testing purposes only
	newUser.SetPassword("password")
	users := db.C("users")
	if err := users.Insert(newUser); err != nil {
		rw.WriteHeader(http.StatusConflict)
		rw.Write([]byte("Duplicate!"))
		return
	}

	j, _ := json.Marshal(newUser)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusCreated)
	rw.Write(j)
}

func UserGet(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	vars := mux.Vars(req)
	nickname := vars["nickname"]

	var user models.User
	if err := db.C("users").Find(bson.M{"nickname": nickname}).One(&user); err != nil {
		log.Print("Some error while finding user")
		panic(err)
	}

	j, _ := json.Marshal(user)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func UserPut(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	currentUser := getCurrentUser(req)

	vars := mux.Vars(req)
	nickname := vars["nickname"]

	var user models.User
	if err := db.C("users").Find(bson.M{"nickname": nickname}).One(&user); err != nil {
		log.Print("Some error while finding user")
		panic(err)
	}

	if currentUser.Nickname != user.Nickname && currentUser.Role != "admin" {
		rw.WriteHeader(http.StatusForbidden)
		return
	}

	decoder := json.NewDecoder(req.Body)
	var updatedUser models.User
	if err := decoder.Decode(&updatedUser); err != nil {
		log.Print("Error decoding updaterUser json")
		panic(err)
	}

	user.FirstName = updatedUser.FirstName
	user.LastName = updatedUser.LastName

	if err := db.C("users").UpdateId(user.Id, user); err != nil {
		log.Print("Error updating user")
		panic(err)
	}
	j, _ := json.Marshal(user)
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(j)
}

func UserDelete(rw http.ResponseWriter, req *http.Request) {
	db := context.Get(req, "db").(*mgo.Database)
	vars := mux.Vars(req)
	nickname := vars["nickname"]

	if err := db.C("users").Remove(bson.M{"nickname": nickname}); err != nil {
		log.Print("Cannot delete user")
		panic(err)
	}
	rw.WriteHeader(http.StatusGone)
}
