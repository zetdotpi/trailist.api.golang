package endpoints

import (
	"github.com/gorilla/context"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"trailist/models"
)

func getClaims(req *http.Request) map[string]interface{} {
	claims := context.Get(req, "claims").(map[string]interface{})
	return claims
}

func getCurrentUser(req *http.Request) models.User {
	db := context.Get(req, "db").(*mgo.Database)
	nickname := getClaims(req)["username"]

	var user models.User
	if err := db.C("users").Find(bson.M{"nickname": nickname}).One(&user); err != nil {
		log.Print("Some error while finding user")
		panic(err)
	}

	return user
}
