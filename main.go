package main

import (
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
	"gopkg.in/mgo.v2"
	"log"
	"net/http"
	"strings"
	"time"
	"trailist/auth"
	"trailist/endpoints"
	"trailist/utils"
)

const (
	DBName string = "trailist"
)

var (
	mongo *mgo.Session
)

func injectDBHandler(next http.Handler) http.Handler {
	fn := func(rw http.ResponseWriter, req *http.Request) {
		mongoCopy := mongo.Clone()
		db := mongoCopy.DB(DBName)
		context.Set(req, "db", db)
		next.ServeHTTP(rw, req)
		mongoCopy.Close()
	}
	return http.HandlerFunc(fn)
}

func logHandler(next http.Handler) http.Handler {
	fn := func(rw http.ResponseWriter, req *http.Request) {
		tStart := time.Now()
		next.ServeHTTP(rw, req)
		tEnd := time.Now()
		log.Printf("[%s] %q %v\n", req.Method, req.URL.String(), tEnd.Sub(tStart))
	}
	return http.HandlerFunc(fn)
}

func requireAdminHandler(next http.Handler) http.Handler {
	fn := func(rw http.ResponseWriter, req *http.Request) {
		claims := context.Get(req, "claims").(map[string]interface{})
		log.Print(claims)
		role := claims["role"]
		if role == "admin" {
			next.ServeHTTP(rw, req)
		} else {
			http.Error(rw, "Admins only", http.StatusForbidden)
		}
	}
	return http.HandlerFunc(fn)
}

func init() {
	// session, err := mgo.Dial("localhost")
	// if err != nil {
	// 	log.Printf("Error on connection")
	// 	panic(err)
	// }
	// db := session.DB(DBName)
	// users := db.C("users")

	// users_email_index := mgo.Index{
	// 	Key:        []string{"email"},
	// 	Unique:     true,
	// 	DropDups:   true,
	// 	Background: true,
	// 	Sparse:     true,
	// }
	// log.Print("Ensuring Index on User.email")
	// if err = users.EnsureIndex(users_email_index); err != nil {
	// 	log.Print("Failed to ensure email index")
	// 	panic(err)
	// }

	// log.Print("Ensuring Index on User.nickname")
	// users_nickname_index := mgo.Index{
	// 	Key:        []string{"nickname"},
	// 	Unique:     true,
	// 	DropDups:   true,
	// 	Background: true,
	// 	Sparse:     true,
	// }
	// if err = users.EnsureIndex(users_nickname_index); err != nil {
	// 	log.Print("Failed to ensure nickname index")
	// 	panic(err)
	// }

	// groups := db.C("groups")
	// log.Print("Ensuring Index on Group.name")
	// groups_name_index := mgo.Index{
	// 	Key:        []string{"name"},
	// 	Unique:     true,
	// 	DropDups:   true,
	// 	Background: true,
	// 	Sparse:     true,
	// }
	// if err = groups.EnsureIndex(groups_name_index); err != nil {
	// 	log.Print("Faild to ensure name index")
	// 	panic(err)
	// }

	// log.Print("Done")
}

// EVENTS CRUD

func main() {
	// loading variables
	env := utils.NewEnvLoader()
	serverPort, err := env.Get("TR_PORT")
	serverAddress, err := env.Get("TR_ADDRESS")
	mongoAddress, err := env.Get("TR_DB_ADDRESS")
	mongoUsername, err := env.Get("TR_DB_USERNAME")
	mongoPassword, err := env.Get("TR_DB_PASSWORD")

	if err != nil {
		panic(err)
	}

	mongoCred := mgo.Credential{
		mongoUsername,
		mongoPassword,
		"",
		"",
		"",
		"",
	}

	mongo, err = mgo.Dial(mongoAddress)
	err = mongo.Login(&mongoCred)
	if err != nil {
		log.Fatal("Error connecting to MongoDB")
		panic(err)
	}

	defer mongo.Close()
	mongo.SetMode(mgo.Monotonic, true)

	log.Print("Setup alice")
	commonHandlers := alice.New(context.ClearHandler, logHandler, injectDBHandler)
	authHandlers := commonHandlers.Extend(alice.New(auth.AuthHandler))
	adminHandlers := authHandlers.Extend(alice.New(requireAdminHandler))

	log.Print("Preparing Gorilla Mux.")
	router := mux.NewRouter()
	// Auth
	router.Handle("/login/", commonHandlers.ThenFunc(auth.LoginHandler)).Methods("POST")
	// TODO: add registration route + handler

	// Users
	users := router.PathPrefix("/users").Subrouter()
	users.Handle("/", authHandlers.ThenFunc(endpoints.UsersGet)).Methods("GET")
	users.Handle("/", adminHandlers.ThenFunc(endpoints.UsersPost)).Methods("POST")
	users.Handle("/{nickname}", authHandlers.ThenFunc(endpoints.UserGet)).Methods("GET")
	users.Handle("/{nickname}", authHandlers.ThenFunc(endpoints.UserPut)).Methods("PUT")
	users.Handle("/{nickname}", adminHandlers.ThenFunc(endpoints.UserDelete)).Methods("DELETE")
	// TODO: add this routes later
	// users.HandleFunc("/{nickname}/deactivate", authHandlers.ThenFunc(DeactivateUser)).Methods("POST")

	groups := router.PathPrefix("/groups").Subrouter()
	groups.Handle("/", authHandlers.ThenFunc(endpoints.GroupsGet)).Methods("GET")
	groups.Handle("/", authHandlers.ThenFunc(endpoints.GroupsPost)).Methods("POST")
	groups.Handle("/{groupName}", authHandlers.ThenFunc(endpoints.GroupGet)).Methods("GET")
	groups.Handle("/{groupName}", authHandlers.ThenFunc(endpoints.GroupPut)).Methods("PUT")
	groups.Handle("/{groupName}", authHandlers.ThenFunc(endpoints.GroupDelete)).Methods("DELETE")

	tracks := router.PathPrefix("/tracks").Subrouter()
	tracks.Handle("/", authHandlers.ThenFunc(endpoints.TracksGet)).Methods("GET")
	tracks.Handle("/", authHandlers.ThenFunc(endpoints.TracksPost)).Methods("POST")
	tracks.Handle("/{trackId}", authHandlers.ThenFunc(endpoints.TrackGet)).Methods("GET")
	// tracks.Handle("/{trackId}", authHandlers.ThenFunc(TrackPut)).Methods("PUT")
	tracks.Handle("/{trackId}", authHandlers.ThenFunc(endpoints.TrackDelete)).Methods("DELETE")

	trails := router.PathPrefix("/trails").Subrouter()
	trails.Handle("/", authHandlers.ThenFunc(endpoints.TrailsGet)).Methods("GET")
	trails.Handle("/", authHandlers.ThenFunc(endpoints.TrailsPost)).Methods("POST")
	trails.Handle("/{trailId}", authHandlers.ThenFunc(endpoints.TrailGet)).Methods("GET")
	trails.Handle("/{trailId}", authHandlers.ThenFunc(endpoints.TrailPut)).Methods("PUT")
	trails.Handle("/{trailId}", authHandlers.ThenFunc(endpoints.TrailDelete)).Methods("DELETE")

	events := router.PathPrefix("/events").Subrouter()
	events.Handle("/", authHandlers.ThenFunc(endpoints.EventsGet)).Methods("GET")
	events.Handle("/", authHandlers.ThenFunc(endpoints.EventsPost)).Methods("POST")
	events.Handle("/{eventId}", authHandlers.ThenFunc(endpoints.EventGet)).Methods("GET")
	events.Handle("/{eventId}", authHandlers.ThenFunc(endpoints.EventPut)).Methods("PUT")
	events.Handle("/{eventId}", authHandlers.ThenFunc(endpoints.EventDelete)).Methods("DELETE")
	// events.Handle("/{eventId}/subscribe", authHandlers.ThenFunc(EventSubscribe)).Methods("POST")
	// events.Handle("/{eventId}/subscribe", authHandlers.ThenFunc(EventUnsubscribe)).Methods("DELETE")

	eventResults := events.PathPrefix("/{eventId}/results").Subrouter()
	eventResults.Handle("/", authHandlers.ThenFunc(endpoints.EventResultsGet)).Methods("GET")
	eventResults.Handle("/", authHandlers.ThenFunc(endpoints.EventResultsPost)).Methods("POST")
	// // TODO: check if DELETE and PUT methods are necessary

	log.Print("Ready. Steady. Go!")

	http.Handle("/", router)
	http.ListenAndServe(strings.Join([]string{serverAddress, serverPort}, ":"), nil)
}
