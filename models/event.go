package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type EventResult struct {
	TrackID bson.ObjectId `json:"trackId"`
	UserID  bson.ObjectId `json:"userId"` // User ID
}

type Event struct {
	Id               bson.ObjectId   `bson:"_id,omitempty" json:"id"`
	AuthorID         bson.ObjectId   `json:"authorId"` // User ID
	CreationDatetime time.Time       `json:"creationDatetime"`
	Name             string          `json:"name"`
	Description      string          `bson:",omitempty" json:"description"`
	TrailID          bson.ObjectId   `json:"trailId"` // Trail ID
	StartDatetime    time.Time       `json:"startDatetime"`
	Types            []string        `json:"types"`
	Participants     []bson.ObjectId `json:"participantsIDs"` // User IDs
	Results          []EventResult   `json:"-"`               // EventResult IDs
}
