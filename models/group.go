package models

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Group struct {
	Id               bson.ObjectId   `bson:"_id,omitempty" json:"-"`
	Name             string          `json:"name"`
	IsPrivate        bool            `json:"isPrivate"`
	CreationDatetime time.Time       `json:"creationDatetime"`                          //
	OwnerID          bson.ObjectId   `bson:",omitempty" json:"ownerID"`                 // User ID
	ModeratorsIDs    []bson.ObjectId `bson:",omitempty" json:"moderatorsIDs,omitempty"` // User IDs
	MembersIDs       []bson.ObjectId `bson:",omitempty" json:"membersIDs,omitempty"`    // User IDs
}
