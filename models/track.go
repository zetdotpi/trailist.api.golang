package models

import (
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type GeoPoint struct {
	Type        string    `json:"-"`
	Coordinates []float64 `json:"coordinates"`
}

func NewGeoPoint(lon, lat float64) GeoPoint {
	if lon < -180. || lon > 180. {
		panic("WHOAH!")
	}
	if lat < -90. || lat > 90. {
		panic("WHOAH!")
	}
	return GeoPoint{"Point", []float64{lon, lat}}
}

func (gp GeoPoint) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Lon float64 `json:"lon"`
		Lat float64 `json:"lat"`
	}{
		gp.Coordinates[0],
		gp.Coordinates[1],
	})
}

type Bounds struct {
	Upper GeoPoint `json:"upper"`
	Lower GeoPoint `json:"lower"`
}

type TrackSummary struct {
	RunTime   time.Duration `bson:",omitempty" json:"runTime"`
	Length    float64       `bson:",omitempty" json:"length"`
	Elevation int32         `bson:",omitempty" json:"elevation"`
	Descent   int32         `bson:",omitempty" json:"descent"`
	Bounds    Bounds        `bson:",omitempty" json:"bounds"`
}

type TrackFile struct {
	Filename string `bson:",omitempty"`
	Location string `bson:",omitempty"`
}

type Track struct {
	Id             bson.ObjectId `bson:"_id,omitempty" json:"id"`
	UploaderID     bson.ObjectId `bson:,omitempty" json:"uploaderID"` // User ID
	UploadDatetime time.Time     `bson:",omitempty" json:"uploadDatetime"`
	TrackFile      TrackFile     `json:"-"`
	Summary        TrackSummary  `bson:",omitempty" json:"summary"`
	Location       GeoPoint      `bson:"location,omitempty" json:"location"`
}
