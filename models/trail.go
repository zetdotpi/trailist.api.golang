package models

import "gopkg.in/mgo.v2/bson"
import "time"

type Trail struct {
	Id               bson.ObjectId `bson:"_id,omitempty" json:"id"`
	AuthorID         bson.ObjectId `bson:",omitempty" json:"authorId,omitempty"`
	CreationDatetime time.Time     `bson:",omitempty" json:"creationDatetime,omitempty"`
	Track            bson.ObjectId `bson:",omitempty" json:"trackId,omitempty"`
	Description      string        `bson:",omitempty" json:"description,omitempty"`
	Types            []string      `bson:",omitempty" json:"types,omitempty"`
}
