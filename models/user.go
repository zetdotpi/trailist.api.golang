package models

import (
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2/bson"
	"log"
	"time"
)

type User struct {
	Id                   bson.ObjectId `bson:"_id,omitempty" json:"-"`
	Email                string        `json:"email"`
	Nickname             string        `json:"nickname"`
	PwdHash              string        `json:"-"`
	RegistrationDatetime time.Time     `json:"registrationDatetime,omitempty"`
	FirstName            string        `json:"firstName"`
	LastName             string        `json:"lastName"`
	Role                 string        `json:'role'`
}

func generatePwdHash(password string) string {
	pwhash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Print("WTF?! Couldn't generate bcrypt from password")
		panic(err)
	}
	return string(pwhash)
}

func NewUser(email string, nickname string, password string, firstName string, lastName string, role string) User {
	u := User{
		bson.NewObjectId(),
		email,
		nickname,
		generatePwdHash(password),
		time.Now(),
		firstName,
		lastName,
		role,
	}

	return u
}

func (u *User) SetPassword(password string) {
	u.PwdHash = generatePwdHash(password)
}

func (u *User) CheckPassword(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.PwdHash), []byte(password))
	if err != nil {
		return false
	}
	return true
}
