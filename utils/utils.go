package utils

import (
	"errors"
	"os"
)

type EnvLoader struct {
	err error
}

func NewEnvLoader() EnvLoader {
	return EnvLoader{nil}
}

func (e *EnvLoader) Get(name string) (string, error) {
	if e.err != nil {
		return "", e.err
	}

	osVar := os.Getenv(name)
	if osVar == "" {
		e.err = errors.New("Cannot get OS variable named " + name)
		return osVar, e.err
	} else {
		return osVar, nil
	}
}
